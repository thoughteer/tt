# Tensor Train (TT)

Modified fork of [ttpy](https://github.com/oseledets/ttpy) compatible with
Python 3.x and [pip](https://pypi.python.org/pypi/pip).

---

## License

**TT** is released under the MIT license.

---

## Installation

This package requires a Fortran 77/90 compiler, MKL or LAPACK, and BLAS.

You can install this package just by running
```console
$ pip install tt
```