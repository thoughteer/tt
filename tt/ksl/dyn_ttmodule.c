/* File: dyn_ttmodule.c
 * This file is auto-generated with f2py (version:2).
 * f2py is a Fortran to Python Interface Generator (FPIG), Second Edition,
 * written by Pearu Peterson <pearu@cens.ioc.ee>.
 * See http://cens.ioc.ee/projects/f2py2e/
 * Generation date: Sun Aug 23 11:58:05 2015
 * $Revision:$
 * $Date:$
 * Do not edit this file directly unless you know what you are doing!!!
 */
#ifdef __cplusplus
extern "C" {
#endif

/*********************** See f2py2e/cfuncs.py: includes ***********************/
#include <stdarg.h>
#include "Python.h"
#include "fortranobject.h"
/*need_includes0*/

/**************** See f2py2e/rules.py: mod_rules['modulebody'] ****************/
static PyObject *dyn_tt_error;
static PyObject *dyn_tt_module;

/*********************** See f2py2e/cfuncs.py: typedefs ***********************/
typedef struct {double r,i;} complex_double;

/****************** See f2py2e/cfuncs.py: typedefs_generated ******************/
/*need_typedefs_generated*/

/********************** See f2py2e/cfuncs.py: cppmacros **********************/
#ifdef DEBUGCFUNCS
#define CFUNCSMESS(mess) fprintf(stderr,"debug-capi:"mess);
#define CFUNCSMESSPY(mess,obj) CFUNCSMESS(mess) \
  PyObject_Print((PyObject *)obj,stderr,Py_PRINT_RAW);\
  fprintf(stderr,"\n");
#else
#define CFUNCSMESS(mess)
#define CFUNCSMESSPY(mess,obj)
#endif

#ifndef max
#define max(a,b) ((a > b) ? (a) : (b))
#endif
#ifndef min
#define min(a,b) ((a < b) ? (a) : (b))
#endif
#ifndef MAX
#define MAX(a,b) ((a > b) ? (a) : (b))
#endif
#ifndef MIN
#define MIN(a,b) ((a < b) ? (a) : (b))
#endif

#define rank(var) var ## _Rank
#define shape(var,dim) var ## _Dims[dim]
#define old_rank(var) (((PyArrayObject *)(capi_ ## var ## _tmp))->nd)
#define old_shape(var,dim) (((PyArrayObject *)(capi_ ## var ## _tmp))->dimensions[dim])
#define fshape(var,dim) shape(var,rank(var)-dim-1)
#define len(var) shape(var,0)
#define flen(var) fshape(var,0)
#define old_size(var) PyArray_SIZE((PyArrayObject *)(capi_ ## var ## _tmp))
/* #define index(i) capi_i ## i */
#define slen(var) capi_ ## var ## _len
#define size(var, ...) f2py_size((PyArrayObject *)(capi_ ## var ## _tmp), ## __VA_ARGS__, -1)

#if defined(PREPEND_FORTRAN)
#if defined(NO_APPEND_FORTRAN)
#if defined(UPPERCASE_FORTRAN)
#define F_FUNC(f,F) _##F
#else
#define F_FUNC(f,F) _##f
#endif
#else
#if defined(UPPERCASE_FORTRAN)
#define F_FUNC(f,F) _##F##_
#else
#define F_FUNC(f,F) _##f##_
#endif
#endif
#else
#if defined(NO_APPEND_FORTRAN)
#if defined(UPPERCASE_FORTRAN)
#define F_FUNC(f,F) F
#else
#define F_FUNC(f,F) f
#endif
#else
#if defined(UPPERCASE_FORTRAN)
#define F_FUNC(f,F) F##_
#else
#define F_FUNC(f,F) f##_
#endif
#endif
#endif
#if defined(UNDERSCORE_G77)
#define F_FUNC_US(f,F) F_FUNC(f##_,F##_)
#else
#define F_FUNC_US(f,F) F_FUNC(f,F)
#endif


/************************ See f2py2e/cfuncs.py: cfuncs ************************/
static int f2py_size(PyArrayObject* var, ...)
{
  npy_int sz = 0;
  npy_int dim;
  npy_int rank;
  va_list argp;
  va_start(argp, var);
  dim = va_arg(argp, npy_int);
  if (dim==-1)
    {
      sz = PyArray_SIZE(var);
    }
  else
    {
      rank = PyArray_NDIM(var);
      if (dim>=1 && dim<=rank)
        sz = PyArray_DIM(var, dim-1);
      else
        fprintf(stderr, "f2py_size: 2nd argument value=%d fails to satisfy 1<=value<=%d. Result will be 0.\n", dim, rank);
    }
  va_end(argp);
  return sz;
}

static int int_from_pyobj(int* v,PyObject *obj,const char *errmess) {
  PyObject* tmp = NULL;
  if (PyInt_Check(obj)) {
    *v = (int)PyInt_AS_LONG(obj);
    return 1;
  }
  tmp = PyNumber_Int(obj);
  if (tmp) {
    *v = PyInt_AS_LONG(tmp);
    Py_DECREF(tmp);
    return 1;
  }
  if (PyComplex_Check(obj))
    tmp = PyObject_GetAttrString(obj,"real");
  else if (PyString_Check(obj) || PyUnicode_Check(obj))
    /*pass*/;
  else if (PySequence_Check(obj))
    tmp = PySequence_GetItem(obj,0);
  if (tmp) {
    PyErr_Clear();
    if (int_from_pyobj(v,tmp,errmess)) {Py_DECREF(tmp); return 1;}
    Py_DECREF(tmp);
  }
  {
    PyObject* err = PyErr_Occurred();
    if (err==NULL) err = dyn_tt_error;
    PyErr_SetString(err,errmess);
  }
  return 0;
}

static int double_from_pyobj(double* v,PyObject *obj,const char *errmess) {
  PyObject* tmp = NULL;
  if (PyFloat_Check(obj)) {
#ifdef __sgi
    *v = PyFloat_AsDouble(obj);
#else
    *v = PyFloat_AS_DOUBLE(obj);
#endif
    return 1;
  }
  tmp = PyNumber_Float(obj);
  if (tmp) {
#ifdef __sgi
    *v = PyFloat_AsDouble(tmp);
#else
    *v = PyFloat_AS_DOUBLE(tmp);
#endif
    Py_DECREF(tmp);
    return 1;
  }
  if (PyComplex_Check(obj))
    tmp = PyObject_GetAttrString(obj,"real");
  else if (PyString_Check(obj) || PyUnicode_Check(obj))
    /*pass*/;
  else if (PySequence_Check(obj))
    tmp = PySequence_GetItem(obj,0);
  if (tmp) {
    PyErr_Clear();
    if (double_from_pyobj(v,tmp,errmess)) {Py_DECREF(tmp); return 1;}
    Py_DECREF(tmp);
  }
  {
    PyObject* err = PyErr_Occurred();
    if (err==NULL) err = dyn_tt_error;
    PyErr_SetString(err,errmess);
  }
  return 0;
}


/********************* See f2py2e/cfuncs.py: userincludes *********************/
/*need_userincludes*/

/********************* See f2py2e/capi_rules.py: usercode *********************/


/* See f2py2e/rules.py */
/*eof externroutines*/

/******************** See f2py2e/capi_rules.py: usercode1 ********************/


/******************* See f2py2e/cb_rules.py: buildcallback *******************/
/*need_callbacks*/

/*********************** See f2py2e/rules.py: buildapi ***********************/

/***************************** deallocate_result *****************************/
static char doc_f2py_rout_dyn_tt_dyn_tt_deallocate_result[] = "\
deallocate_result()\n\nWrapper for ``deallocate_result``.\
\n";
/*  */
static PyObject *f2py_rout_dyn_tt_dyn_tt_deallocate_result(const PyObject *capi_self,
                           PyObject *capi_args,
                           PyObject *capi_keywds,
                           void (*f2py_func)(void)) {
  PyObject * volatile capi_buildvalue = NULL;
  volatile int f2py_success = 1;
/*decl*/

  static char *capi_kwlist[] = {NULL};

/*routdebugenter*/
#ifdef F2PY_REPORT_ATEXIT
f2py_start_clock();
#endif
  if (!PyArg_ParseTupleAndKeywords(capi_args,capi_keywds,\
    ":dyn_tt.dyn_tt.deallocate_result",\
    capi_kwlist))
    return NULL;
/*frompyobj*/
/*end of frompyobj*/
#ifdef F2PY_REPORT_ATEXIT
f2py_start_call_clock();
#endif
/*callfortranroutine*/
        (*f2py_func)();
if (PyErr_Occurred())
  f2py_success = 0;
#ifdef F2PY_REPORT_ATEXIT
f2py_stop_call_clock();
#endif
/*end of callfortranroutine*/
    if (f2py_success) {
/*pyobjfrom*/
/*end of pyobjfrom*/
    CFUNCSMESS("Building return value.\n");
    capi_buildvalue = Py_BuildValue("");
/*closepyobjfrom*/
/*end of closepyobjfrom*/
    } /*if (f2py_success) after callfortranroutine*/
/*cleanupfrompyobj*/
/*end of cleanupfrompyobj*/
  if (capi_buildvalue == NULL) {
/*routdebugfailure*/
  } else {
/*routdebugleave*/
  }
  CFUNCSMESS("Freeing memory.\n");
/*freemem*/
#ifdef F2PY_REPORT_ATEXIT
f2py_stop_clock();
#endif
  return capi_buildvalue;
}
/************************** end of deallocate_result **************************/

/*********************************** tt_ksl ***********************************/
static char doc_f2py_rout_dyn_tt_dyn_tt_tt_ksl[] = "\
tt_ksl(d,n,m,ra,cra,cry0,ry,tau,rmax,[kickrank,nswp,verb])\n\nWrapper for ``tt_ksl``.\
\n\nParameters\n----------\n"
"d : input int\n"
"n : input rank-1 array('i') with bounds (*)\n"
"m : input rank-1 array('i') with bounds (*)\n"
"ra : input rank-1 array('i') with bounds (*)\n"
"cra : input rank-1 array('d') with bounds (*)\n"
"cry0 : input rank-1 array('d') with bounds (*)\n"
"ry : in/output rank-1 array('i') with bounds (*)\n"
"tau : input float\n"
"rmax : input int\n"
"\nOther Parameters\n----------------\n"
"kickrank : input int\n"
"nswp : input int\n"
"verb : input int";
/*  */
static PyObject *f2py_rout_dyn_tt_dyn_tt_tt_ksl(const PyObject *capi_self,
                           PyObject *capi_args,
                           PyObject *capi_keywds,
                           void (*f2py_func)(int*,int*,int*,int*,double*,double*,int*,double*,int*,int*,int*,int*)) {
  PyObject * volatile capi_buildvalue = NULL;
  volatile int f2py_success = 1;
/*decl*/

  int d = 0;
  PyObject *d_capi = Py_None;
  int *n = NULL;
  npy_intp n_Dims[1] = {-1};
  const int n_Rank = 1;
  PyArrayObject *capi_n_tmp = NULL;
  int capi_n_intent = 0;
  PyObject *n_capi = Py_None;
  int *m = NULL;
  npy_intp m_Dims[1] = {-1};
  const int m_Rank = 1;
  PyArrayObject *capi_m_tmp = NULL;
  int capi_m_intent = 0;
  PyObject *m_capi = Py_None;
  int *ra = NULL;
  npy_intp ra_Dims[1] = {-1};
  const int ra_Rank = 1;
  PyArrayObject *capi_ra_tmp = NULL;
  int capi_ra_intent = 0;
  PyObject *ra_capi = Py_None;
  double *cra = NULL;
  npy_intp cra_Dims[1] = {-1};
  const int cra_Rank = 1;
  PyArrayObject *capi_cra_tmp = NULL;
  int capi_cra_intent = 0;
  PyObject *cra_capi = Py_None;
  double *cry0 = NULL;
  npy_intp cry0_Dims[1] = {-1};
  const int cry0_Rank = 1;
  PyArrayObject *capi_cry0_tmp = NULL;
  int capi_cry0_intent = 0;
  PyObject *cry0_capi = Py_None;
  int *ry = NULL;
  npy_intp ry_Dims[1] = {-1};
  const int ry_Rank = 1;
  PyArrayObject *capi_ry_tmp = NULL;
  int capi_ry_intent = 0;
  PyObject *ry_capi = Py_None;
  double tau = 0;
  PyObject *tau_capi = Py_None;
  int rmax = 0;
  PyObject *rmax_capi = Py_None;
  int kickrank = 0;
  PyObject *kickrank_capi = Py_None;
  int nswp = 0;
  PyObject *nswp_capi = Py_None;
  int verb = 0;
  PyObject *verb_capi = Py_None;
  static char *capi_kwlist[] = {"d","n","m","ra","cra","cry0","ry","tau","rmax","kickrank","nswp","verb",NULL};

/*routdebugenter*/
#ifdef F2PY_REPORT_ATEXIT
f2py_start_clock();
#endif
  if (!PyArg_ParseTupleAndKeywords(capi_args,capi_keywds,\
    "OOOOOOOOO|OOO:dyn_tt.dyn_tt.tt_ksl",\
    capi_kwlist,&d_capi,&n_capi,&m_capi,&ra_capi,&cra_capi,&cry0_capi,&ry_capi,&tau_capi,&rmax_capi,&kickrank_capi,&nswp_capi,&verb_capi))
    return NULL;
/*frompyobj*/
  /* Processing variable ry */
  ;
  capi_ry_intent |= F2PY_INTENT_INOUT;
  capi_ry_tmp = array_from_pyobj(NPY_INT,ry_Dims,ry_Rank,capi_ry_intent,ry_capi);
  if (capi_ry_tmp == NULL) {
    if (!PyErr_Occurred())
      PyErr_SetString(dyn_tt_error,"failed in converting 7th argument `ry' of dyn_tt.dyn_tt.tt_ksl to C/Fortran array" );
  } else {
    ry = (int *)(capi_ry_tmp->data);

  /* Processing variable verb */
  if (verb_capi != Py_None)
    f2py_success = int_from_pyobj(&verb,verb_capi,"dyn_tt.dyn_tt.tt_ksl() 3rd keyword (verb) can't be converted to int");
  if (f2py_success) {
  /* Processing variable nswp */
  if (nswp_capi != Py_None)
    f2py_success = int_from_pyobj(&nswp,nswp_capi,"dyn_tt.dyn_tt.tt_ksl() 2nd keyword (nswp) can't be converted to int");
  if (f2py_success) {
  /* Processing variable kickrank */
  if (kickrank_capi != Py_None)
    f2py_success = int_from_pyobj(&kickrank,kickrank_capi,"dyn_tt.dyn_tt.tt_ksl() 1st keyword (kickrank) can't be converted to int");
  if (f2py_success) {
  /* Processing variable d */
    f2py_success = int_from_pyobj(&d,d_capi,"dyn_tt.dyn_tt.tt_ksl() 1st argument (d) can't be converted to int");
  if (f2py_success) {
  /* Processing variable ra */
  ;
  capi_ra_intent |= F2PY_INTENT_IN;
  capi_ra_tmp = array_from_pyobj(NPY_INT,ra_Dims,ra_Rank,capi_ra_intent,ra_capi);
  if (capi_ra_tmp == NULL) {
    if (!PyErr_Occurred())
      PyErr_SetString(dyn_tt_error,"failed in converting 4th argument `ra' of dyn_tt.dyn_tt.tt_ksl to C/Fortran array" );
  } else {
    ra = (int *)(capi_ra_tmp->data);

  /* Processing variable tau */
    f2py_success = double_from_pyobj(&tau,tau_capi,"dyn_tt.dyn_tt.tt_ksl() 8th argument (tau) can't be converted to double");
  if (f2py_success) {
  /* Processing variable cry0 */
  ;
  capi_cry0_intent |= F2PY_INTENT_IN;
  capi_cry0_tmp = array_from_pyobj(NPY_DOUBLE,cry0_Dims,cry0_Rank,capi_cry0_intent,cry0_capi);
  if (capi_cry0_tmp == NULL) {
    if (!PyErr_Occurred())
      PyErr_SetString(dyn_tt_error,"failed in converting 6th argument `cry0' of dyn_tt.dyn_tt.tt_ksl to C/Fortran array" );
  } else {
    cry0 = (double *)(capi_cry0_tmp->data);

  /* Processing variable n */
  ;
  capi_n_intent |= F2PY_INTENT_IN;
  capi_n_tmp = array_from_pyobj(NPY_INT,n_Dims,n_Rank,capi_n_intent,n_capi);
  if (capi_n_tmp == NULL) {
    if (!PyErr_Occurred())
      PyErr_SetString(dyn_tt_error,"failed in converting 2nd argument `n' of dyn_tt.dyn_tt.tt_ksl to C/Fortran array" );
  } else {
    n = (int *)(capi_n_tmp->data);

  /* Processing variable cra */
  ;
  capi_cra_intent |= F2PY_INTENT_IN;
  capi_cra_tmp = array_from_pyobj(NPY_DOUBLE,cra_Dims,cra_Rank,capi_cra_intent,cra_capi);
  if (capi_cra_tmp == NULL) {
    if (!PyErr_Occurred())
      PyErr_SetString(dyn_tt_error,"failed in converting 5th argument `cra' of dyn_tt.dyn_tt.tt_ksl to C/Fortran array" );
  } else {
    cra = (double *)(capi_cra_tmp->data);

  /* Processing variable rmax */
    f2py_success = int_from_pyobj(&rmax,rmax_capi,"dyn_tt.dyn_tt.tt_ksl() 9th argument (rmax) can't be converted to int");
  if (f2py_success) {
  /* Processing variable m */
  ;
  capi_m_intent |= F2PY_INTENT_IN;
  capi_m_tmp = array_from_pyobj(NPY_INT,m_Dims,m_Rank,capi_m_intent,m_capi);
  if (capi_m_tmp == NULL) {
    if (!PyErr_Occurred())
      PyErr_SetString(dyn_tt_error,"failed in converting 3rd argument `m' of dyn_tt.dyn_tt.tt_ksl to C/Fortran array" );
  } else {
    m = (int *)(capi_m_tmp->data);

/*end of frompyobj*/
#ifdef F2PY_REPORT_ATEXIT
f2py_start_call_clock();
#endif
/*callfortranroutine*/
        (*f2py_func)(&d,n,m,ra,cra,cry0,ry,&tau,&rmax,&kickrank,&nswp,&verb);
if (PyErr_Occurred())
  f2py_success = 0;
#ifdef F2PY_REPORT_ATEXIT
f2py_stop_call_clock();
#endif
/*end of callfortranroutine*/
    if (f2py_success) {
/*pyobjfrom*/
/*end of pyobjfrom*/
    CFUNCSMESS("Building return value.\n");
    capi_buildvalue = Py_BuildValue("");
/*closepyobjfrom*/
/*end of closepyobjfrom*/
    } /*if (f2py_success) after callfortranroutine*/
/*cleanupfrompyobj*/
  if((PyObject *)capi_m_tmp!=m_capi) {
    Py_XDECREF(capi_m_tmp); }
  }  /*if (capi_m_tmp == NULL) ... else of m*/
  /* End of cleaning variable m */
  } /*if (f2py_success) of rmax*/
  /* End of cleaning variable rmax */
  if((PyObject *)capi_cra_tmp!=cra_capi) {
    Py_XDECREF(capi_cra_tmp); }
  }  /*if (capi_cra_tmp == NULL) ... else of cra*/
  /* End of cleaning variable cra */
  if((PyObject *)capi_n_tmp!=n_capi) {
    Py_XDECREF(capi_n_tmp); }
  }  /*if (capi_n_tmp == NULL) ... else of n*/
  /* End of cleaning variable n */
  if((PyObject *)capi_cry0_tmp!=cry0_capi) {
    Py_XDECREF(capi_cry0_tmp); }
  }  /*if (capi_cry0_tmp == NULL) ... else of cry0*/
  /* End of cleaning variable cry0 */
  } /*if (f2py_success) of tau*/
  /* End of cleaning variable tau */
  if((PyObject *)capi_ra_tmp!=ra_capi) {
    Py_XDECREF(capi_ra_tmp); }
  }  /*if (capi_ra_tmp == NULL) ... else of ra*/
  /* End of cleaning variable ra */
  } /*if (f2py_success) of d*/
  /* End of cleaning variable d */
  } /*if (f2py_success) of kickrank*/
  /* End of cleaning variable kickrank */
  } /*if (f2py_success) of nswp*/
  /* End of cleaning variable nswp */
  } /*if (f2py_success) of verb*/
  /* End of cleaning variable verb */
  if((PyObject *)capi_ry_tmp!=ry_capi) {
    Py_XDECREF(capi_ry_tmp); }
  }  /*if (capi_ry_tmp == NULL) ... else of ry*/
  /* End of cleaning variable ry */
/*end of cleanupfrompyobj*/
  if (capi_buildvalue == NULL) {
/*routdebugfailure*/
  } else {
/*routdebugleave*/
  }
  CFUNCSMESS("Freeing memory.\n");
/*freemem*/
#ifdef F2PY_REPORT_ATEXIT
f2py_stop_clock();
#endif
  return capi_buildvalue;
}
/******************************* end of tt_ksl *******************************/

/********************************** ztt_ksl **********************************/
static char doc_f2py_rout_dyn_tt_dyn_tt_ztt_ksl[] = "\
ztt_ksl(d,n,m,ra,cra,cry0,ry,tau,rmax,[kickrank,nswp,verb,typ0,order0])\n\nWrapper for ``ztt_ksl``.\
\n\nParameters\n----------\n"
"d : input int\n"
"n : input rank-1 array('i') with bounds (*)\n"
"m : input rank-1 array('i') with bounds (*)\n"
"ra : input rank-1 array('i') with bounds (*)\n"
"cra : input rank-1 array('D') with bounds (*)\n"
"cry0 : input rank-1 array('D') with bounds (*)\n"
"ry : in/output rank-1 array('i') with bounds (*)\n"
"tau : input float\n"
"rmax : input int\n"
"\nOther Parameters\n----------------\n"
"kickrank : input int\n"
"nswp : input int\n"
"verb : input int\n"
"typ0 : input int\n"
"order0 : input int";
/*  */
static PyObject *f2py_rout_dyn_tt_dyn_tt_ztt_ksl(const PyObject *capi_self,
                           PyObject *capi_args,
                           PyObject *capi_keywds,
                           void (*f2py_func)(int*,int*,int*,int*,complex_double*,complex_double*,int*,double*,int*,int*,int*,int*,int*,int*)) {
  PyObject * volatile capi_buildvalue = NULL;
  volatile int f2py_success = 1;
/*decl*/

  int d = 0;
  PyObject *d_capi = Py_None;
  int *n = NULL;
  npy_intp n_Dims[1] = {-1};
  const int n_Rank = 1;
  PyArrayObject *capi_n_tmp = NULL;
  int capi_n_intent = 0;
  PyObject *n_capi = Py_None;
  int *m = NULL;
  npy_intp m_Dims[1] = {-1};
  const int m_Rank = 1;
  PyArrayObject *capi_m_tmp = NULL;
  int capi_m_intent = 0;
  PyObject *m_capi = Py_None;
  int *ra = NULL;
  npy_intp ra_Dims[1] = {-1};
  const int ra_Rank = 1;
  PyArrayObject *capi_ra_tmp = NULL;
  int capi_ra_intent = 0;
  PyObject *ra_capi = Py_None;
  complex_double *cra = NULL;
  npy_intp cra_Dims[1] = {-1};
  const int cra_Rank = 1;
  PyArrayObject *capi_cra_tmp = NULL;
  int capi_cra_intent = 0;
  PyObject *cra_capi = Py_None;
  complex_double *cry0 = NULL;
  npy_intp cry0_Dims[1] = {-1};
  const int cry0_Rank = 1;
  PyArrayObject *capi_cry0_tmp = NULL;
  int capi_cry0_intent = 0;
  PyObject *cry0_capi = Py_None;
  int *ry = NULL;
  npy_intp ry_Dims[1] = {-1};
  const int ry_Rank = 1;
  PyArrayObject *capi_ry_tmp = NULL;
  int capi_ry_intent = 0;
  PyObject *ry_capi = Py_None;
  double tau = 0;
  PyObject *tau_capi = Py_None;
  int rmax = 0;
  PyObject *rmax_capi = Py_None;
  int kickrank = 0;
  PyObject *kickrank_capi = Py_None;
  int nswp = 0;
  PyObject *nswp_capi = Py_None;
  int verb = 0;
  PyObject *verb_capi = Py_None;
  int typ0 = 0;
  PyObject *typ0_capi = Py_None;
  int order0 = 0;
  PyObject *order0_capi = Py_None;
  static char *capi_kwlist[] = {"d","n","m","ra","cra","cry0","ry","tau","rmax","kickrank","nswp","verb","typ0","order0",NULL};

/*routdebugenter*/
#ifdef F2PY_REPORT_ATEXIT
f2py_start_clock();
#endif
  if (!PyArg_ParseTupleAndKeywords(capi_args,capi_keywds,\
    "OOOOOOOOO|OOOOO:dyn_tt.dyn_tt.ztt_ksl",\
    capi_kwlist,&d_capi,&n_capi,&m_capi,&ra_capi,&cra_capi,&cry0_capi,&ry_capi,&tau_capi,&rmax_capi,&kickrank_capi,&nswp_capi,&verb_capi,&typ0_capi,&order0_capi))
    return NULL;
/*frompyobj*/
  /* Processing variable ry */
  ;
  capi_ry_intent |= F2PY_INTENT_INOUT;
  capi_ry_tmp = array_from_pyobj(NPY_INT,ry_Dims,ry_Rank,capi_ry_intent,ry_capi);
  if (capi_ry_tmp == NULL) {
    if (!PyErr_Occurred())
      PyErr_SetString(dyn_tt_error,"failed in converting 7th argument `ry' of dyn_tt.dyn_tt.ztt_ksl to C/Fortran array" );
  } else {
    ry = (int *)(capi_ry_tmp->data);

  /* Processing variable order0 */
  if (order0_capi != Py_None)
    f2py_success = int_from_pyobj(&order0,order0_capi,"dyn_tt.dyn_tt.ztt_ksl() 5th keyword (order0) can't be converted to int");
  if (f2py_success) {
  /* Processing variable nswp */
  if (nswp_capi != Py_None)
    f2py_success = int_from_pyobj(&nswp,nswp_capi,"dyn_tt.dyn_tt.ztt_ksl() 2nd keyword (nswp) can't be converted to int");
  if (f2py_success) {
  /* Processing variable kickrank */
  if (kickrank_capi != Py_None)
    f2py_success = int_from_pyobj(&kickrank,kickrank_capi,"dyn_tt.dyn_tt.ztt_ksl() 1st keyword (kickrank) can't be converted to int");
  if (f2py_success) {
  /* Processing variable d */
    f2py_success = int_from_pyobj(&d,d_capi,"dyn_tt.dyn_tt.ztt_ksl() 1st argument (d) can't be converted to int");
  if (f2py_success) {
  /* Processing variable ra */
  ;
  capi_ra_intent |= F2PY_INTENT_IN;
  capi_ra_tmp = array_from_pyobj(NPY_INT,ra_Dims,ra_Rank,capi_ra_intent,ra_capi);
  if (capi_ra_tmp == NULL) {
    if (!PyErr_Occurred())
      PyErr_SetString(dyn_tt_error,"failed in converting 4th argument `ra' of dyn_tt.dyn_tt.ztt_ksl to C/Fortran array" );
  } else {
    ra = (int *)(capi_ra_tmp->data);

  /* Processing variable tau */
    f2py_success = double_from_pyobj(&tau,tau_capi,"dyn_tt.dyn_tt.ztt_ksl() 8th argument (tau) can't be converted to double");
  if (f2py_success) {
  /* Processing variable cry0 */
  ;
  capi_cry0_intent |= F2PY_INTENT_IN;
  capi_cry0_tmp = array_from_pyobj(NPY_CDOUBLE,cry0_Dims,cry0_Rank,capi_cry0_intent,cry0_capi);
  if (capi_cry0_tmp == NULL) {
    if (!PyErr_Occurred())
      PyErr_SetString(dyn_tt_error,"failed in converting 6th argument `cry0' of dyn_tt.dyn_tt.ztt_ksl to C/Fortran array" );
  } else {
    cry0 = (complex_double *)(capi_cry0_tmp->data);

  /* Processing variable n */
  ;
  capi_n_intent |= F2PY_INTENT_IN;
  capi_n_tmp = array_from_pyobj(NPY_INT,n_Dims,n_Rank,capi_n_intent,n_capi);
  if (capi_n_tmp == NULL) {
    if (!PyErr_Occurred())
      PyErr_SetString(dyn_tt_error,"failed in converting 2nd argument `n' of dyn_tt.dyn_tt.ztt_ksl to C/Fortran array" );
  } else {
    n = (int *)(capi_n_tmp->data);

  /* Processing variable cra */
  ;
  capi_cra_intent |= F2PY_INTENT_IN;
  capi_cra_tmp = array_from_pyobj(NPY_CDOUBLE,cra_Dims,cra_Rank,capi_cra_intent,cra_capi);
  if (capi_cra_tmp == NULL) {
    if (!PyErr_Occurred())
      PyErr_SetString(dyn_tt_error,"failed in converting 5th argument `cra' of dyn_tt.dyn_tt.ztt_ksl to C/Fortran array" );
  } else {
    cra = (complex_double *)(capi_cra_tmp->data);

  /* Processing variable rmax */
    f2py_success = int_from_pyobj(&rmax,rmax_capi,"dyn_tt.dyn_tt.ztt_ksl() 9th argument (rmax) can't be converted to int");
  if (f2py_success) {
  /* Processing variable typ0 */
  if (typ0_capi != Py_None)
    f2py_success = int_from_pyobj(&typ0,typ0_capi,"dyn_tt.dyn_tt.ztt_ksl() 4th keyword (typ0) can't be converted to int");
  if (f2py_success) {
  /* Processing variable m */
  ;
  capi_m_intent |= F2PY_INTENT_IN;
  capi_m_tmp = array_from_pyobj(NPY_INT,m_Dims,m_Rank,capi_m_intent,m_capi);
  if (capi_m_tmp == NULL) {
    if (!PyErr_Occurred())
      PyErr_SetString(dyn_tt_error,"failed in converting 3rd argument `m' of dyn_tt.dyn_tt.ztt_ksl to C/Fortran array" );
  } else {
    m = (int *)(capi_m_tmp->data);

  /* Processing variable verb */
  if (verb_capi != Py_None)
    f2py_success = int_from_pyobj(&verb,verb_capi,"dyn_tt.dyn_tt.ztt_ksl() 3rd keyword (verb) can't be converted to int");
  if (f2py_success) {
/*end of frompyobj*/
#ifdef F2PY_REPORT_ATEXIT
f2py_start_call_clock();
#endif
/*callfortranroutine*/
        (*f2py_func)(&d,n,m,ra,cra,cry0,ry,&tau,&rmax,&kickrank,&nswp,&verb,&typ0,&order0);
if (PyErr_Occurred())
  f2py_success = 0;
#ifdef F2PY_REPORT_ATEXIT
f2py_stop_call_clock();
#endif
/*end of callfortranroutine*/
    if (f2py_success) {
/*pyobjfrom*/
/*end of pyobjfrom*/
    CFUNCSMESS("Building return value.\n");
    capi_buildvalue = Py_BuildValue("");
/*closepyobjfrom*/
/*end of closepyobjfrom*/
    } /*if (f2py_success) after callfortranroutine*/
/*cleanupfrompyobj*/
  } /*if (f2py_success) of verb*/
  /* End of cleaning variable verb */
  if((PyObject *)capi_m_tmp!=m_capi) {
    Py_XDECREF(capi_m_tmp); }
  }  /*if (capi_m_tmp == NULL) ... else of m*/
  /* End of cleaning variable m */
  } /*if (f2py_success) of typ0*/
  /* End of cleaning variable typ0 */
  } /*if (f2py_success) of rmax*/
  /* End of cleaning variable rmax */
  if((PyObject *)capi_cra_tmp!=cra_capi) {
    Py_XDECREF(capi_cra_tmp); }
  }  /*if (capi_cra_tmp == NULL) ... else of cra*/
  /* End of cleaning variable cra */
  if((PyObject *)capi_n_tmp!=n_capi) {
    Py_XDECREF(capi_n_tmp); }
  }  /*if (capi_n_tmp == NULL) ... else of n*/
  /* End of cleaning variable n */
  if((PyObject *)capi_cry0_tmp!=cry0_capi) {
    Py_XDECREF(capi_cry0_tmp); }
  }  /*if (capi_cry0_tmp == NULL) ... else of cry0*/
  /* End of cleaning variable cry0 */
  } /*if (f2py_success) of tau*/
  /* End of cleaning variable tau */
  if((PyObject *)capi_ra_tmp!=ra_capi) {
    Py_XDECREF(capi_ra_tmp); }
  }  /*if (capi_ra_tmp == NULL) ... else of ra*/
  /* End of cleaning variable ra */
  } /*if (f2py_success) of d*/
  /* End of cleaning variable d */
  } /*if (f2py_success) of kickrank*/
  /* End of cleaning variable kickrank */
  } /*if (f2py_success) of nswp*/
  /* End of cleaning variable nswp */
  } /*if (f2py_success) of order0*/
  /* End of cleaning variable order0 */
  if((PyObject *)capi_ry_tmp!=ry_capi) {
    Py_XDECREF(capi_ry_tmp); }
  }  /*if (capi_ry_tmp == NULL) ... else of ry*/
  /* End of cleaning variable ry */
/*end of cleanupfrompyobj*/
  if (capi_buildvalue == NULL) {
/*routdebugfailure*/
  } else {
/*routdebugleave*/
  }
  CFUNCSMESS("Freeing memory.\n");
/*freemem*/
#ifdef F2PY_REPORT_ATEXIT
f2py_stop_clock();
#endif
  return capi_buildvalue;
}
/******************************* end of ztt_ksl *******************************/
/*eof body*/

/******************* See f2py2e/f90mod_rules.py: buildhooks *******************/

static FortranDataDef f2py_dyn_tt_def[] = {
  {"result_core",1,{{-1}},NPY_DOUBLE},
  {"zresult_core",1,{{-1}},NPY_CDOUBLE},
  {"deallocate_result",-1,{{-1}},0,NULL,(void *)f2py_rout_dyn_tt_dyn_tt_deallocate_result,doc_f2py_rout_dyn_tt_dyn_tt_deallocate_result},
  {"tt_ksl",-1,{{-1}},0,NULL,(void *)f2py_rout_dyn_tt_dyn_tt_tt_ksl,doc_f2py_rout_dyn_tt_dyn_tt_tt_ksl},
  {"ztt_ksl",-1,{{-1}},0,NULL,(void *)f2py_rout_dyn_tt_dyn_tt_ztt_ksl,doc_f2py_rout_dyn_tt_dyn_tt_ztt_ksl},
  {NULL}
};

static void f2py_setup_dyn_tt(void (*result_core)(int*,int*,void(*)(char*,int*),int*),void (*zresult_core)(int*,int*,void(*)(char*,int*),int*),char *deallocate_result,char *tt_ksl,char *ztt_ksl) {
  int i_f2py=0;
  f2py_dyn_tt_def[i_f2py++].func = result_core;
  f2py_dyn_tt_def[i_f2py++].func = zresult_core;
  f2py_dyn_tt_def[i_f2py++].data = deallocate_result;
  f2py_dyn_tt_def[i_f2py++].data = tt_ksl;
  f2py_dyn_tt_def[i_f2py++].data = ztt_ksl;
}
extern void F_FUNC_US(f2pyinitdyn_tt,F2PYINITDYN_TT)(void (*)(void (*)(int*,int*,void(*)(char*,int*),int*),void (*)(int*,int*,void(*)(char*,int*),int*),char *,char *,char *));
static void f2py_init_dyn_tt(void) {
  F_FUNC_US(f2pyinitdyn_tt,F2PYINITDYN_TT)(f2py_setup_dyn_tt);
}

/*need_f90modhooks*/

/************** See f2py2e/rules.py: module_rules['modulebody'] **************/

/******************* See f2py2e/common_rules.py: buildhooks *******************/

/*need_commonhooks*/

/**************************** See f2py2e/rules.py ****************************/

static FortranDataDef f2py_routine_defs[] = {

/*eof routine_defs*/
  {NULL}
};

static PyMethodDef f2py_module_methods[] = {

  {NULL,NULL}
};

#if PY_VERSION_HEX >= 0x03000000
static struct PyModuleDef moduledef = {
  PyModuleDef_HEAD_INIT,
  "dyn_tt",
  NULL,
  -1,
  f2py_module_methods,
  NULL,
  NULL,
  NULL,
  NULL
};
#endif

#if PY_VERSION_HEX >= 0x03000000
#define RETVAL m
PyMODINIT_FUNC PyInit_dyn_tt(void) {
#else
#define RETVAL
PyMODINIT_FUNC initdyn_tt(void) {
#endif
  int i;
  PyObject *m,*d, *s;
#if PY_VERSION_HEX >= 0x03000000
  m = dyn_tt_module = PyModule_Create(&moduledef);
#else
  m = dyn_tt_module = Py_InitModule("dyn_tt", f2py_module_methods);
#endif
  Py_TYPE(&PyFortran_Type) = &PyType_Type;
  import_array();
  if (PyErr_Occurred())
    {PyErr_SetString(PyExc_ImportError, "can't initialize module dyn_tt (failed to import numpy)"); return RETVAL;}
  d = PyModule_GetDict(m);
  s = PyString_FromString("$Revision: $");
  PyDict_SetItemString(d, "__version__", s);
#if PY_VERSION_HEX >= 0x03000000
  s = PyUnicode_FromString(
#else
  s = PyString_FromString(
#endif
    "This module 'dyn_tt' is auto-generated with f2py (version:2).\nFunctions:\n"
"Fortran 90/95 modules:\n""  dyn_tt --- result_core,zresult_core,deallocate_result(),tt_ksl(),ztt_ksl()"".");
  PyDict_SetItemString(d, "__doc__", s);
  dyn_tt_error = PyErr_NewException ("dyn_tt.error", NULL, NULL);
  Py_DECREF(s);
  for(i=0;f2py_routine_defs[i].name!=NULL;i++)
    PyDict_SetItemString(d, f2py_routine_defs[i].name,PyFortranObject_NewAsAttr(&f2py_routine_defs[i]));



/*eof initf2pywraphooks*/
  PyDict_SetItemString(d, "dyn_tt", PyFortranObject_New(f2py_dyn_tt_def,f2py_init_dyn_tt));
/*eof initf90modhooks*/

/*eof initcommonhooks*/


#ifdef F2PY_REPORT_ATEXIT
  if (! PyErr_Occurred())
    on_exit(f2py_report_on_exit,(void*)"dyn_tt");
#endif

  return RETVAL;
}
#ifdef __cplusplus
}
#endif
